CSS Names
================================================================================
CSS Names is a collection of small modules that make it possible to add theming 
to items that were previously difficult to target with css selectors. Most of 
these modules just add additional class names to a particular HTML element, with 
the class name based on the element's content or position within the parent 
structure.

NOTE: the menu_css_names module is also available as a standalone module 
(http://drupal.org/project/menu_css_names), identical to the one included here. 
Please make sure you only add one or the other to your sites/xxx/modules 
directory.



Configuration
================================================================================
No configuration is needed (or provided for) any of the included modules,
although this may change in the future, as more complex modules are added to
CSS Names. To keep things simple, only enable the modules that you need for your
project (you don't need uc_attribute_css enabled if you aren't using Ubercart).



cck_css
--------------------------------------------------------------------------------
This module adds additional classes to CCK fields containing multiple values, 
allowing you to target particular values with css. This is accomplished by
overriding the content-field.tpl.php template file provided with CCK. If you
need to create field-specific template files, just copy this module's 
content-field.tpl.php file and add it to your theme directory.

With cck_css enabled, we get a field-item-X class for each field item:

<div class="field field-type-text field-field-frob-frobozz">
	<div class="field-label">Frobozz:&nbsp;</div>
	<div class="field-items">
    <div class="field-item field-item-1 odd first">Item 1</div>
    <div class="field-item field-item-2 even last">Item 2</div>
  </div>
</div>



menu_css_names
--------------------------------------------------------------------------------
This module adds additional classes to Drupal built-in menus, as well as menus
provided by Nice Menus, and local task menus (tabs). See the module's README
for more info.



uc_attribute_css
--------------------------------------------------------------------------------
This module adds an additional class to each Ubercart attribute, based on the
name of the attribute.

For example, a product has two attributes called "Color" and "Size". With the
uc_attribute_css module enabled, classes are added for each (attribute-color and
attribute-size):

<div class="attributes">
	<div class="attribute attribute-1 attribute-color odd">
   ...
	</div>
	
	<div class="attribute attribute-2 attribute-size even">
	 ...
	</div>
</div>



views_rows_css
--------------------------------------------------------------------------------
This module adds additional classes to table displays in Views, allowing 
specific rows in a table to be targeted with css. This is accomplished by 
overriding the views-view-table.tpl.php template file provided by the Views
module. If you need to create template files for specific table display views,
copy the views-view-table.tpl.php file into your theme directory (don't copy the
code provided by the Information link in the Views UI, as it just shows the 
default Views template code).

With the views_rows_css module enabled, we get a views-row-X class for each row
in the table:

<table class="views-table">
  ...
	<tr class="views-row-1 odd views-row-first">...</tr>
  <tr class="views-row-2 even">...</tr>
	<tr class="views-row-3 odd views-row-last">...</tr>
  ...
</table>
		